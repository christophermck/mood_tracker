# README #

This is the Mood Tracker app created by Christopher McKavanagh (B00663674) for COM555 Assessment 2. The purpose of the app is to capture the user's current mood, save it to a local database and display the historical results in a graph. The user preferences screen is part of the extended requirements allowing the user to enable dark mode as well as set up reminders to help notify the user that it's time to enter their current mood. I decided to use the generic app specification provided, instead of using my own specification I made for Assessment 1. This is because I believe I outlined too many requirements, so the scope was too large to implement. Sticking with the generic app specification allowed a more reasonable scope to be defined and implemented.

### How do I get set up? ###

#### Required Hardware & Software: ####

* Macbook running Xcode Version 9.3 (9E145)
* iPhone X running iOS 11.3
* Use cocoapods to install [swiftCharts](https://github.com/i-schuetz/SwiftCharts) to the root of the project directory

#### To open and run the project: ####

* Open Mood Tracker.xcworkspace in Xcode
* Point the build at the connected iPhone X device
* Press command + R to run the project

### Who do I talk to? ###

Repo owner Christopher McKavanagh (mckavanagh-c@ulster.ac.uk)