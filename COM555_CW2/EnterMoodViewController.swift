//
//  EnterMoodViewController.swift
//  COM555_CW2
//
//  Created by Christopher McKavanagh on 27/03/2018.
//  Copyright © 2018 Christopher McKavanagh. All rights reserved.
//

import UIKit
import SQLite3
import CoreLocation

class EnterMoodViewController: UIViewController, CLLocationManagerDelegate {
    
    var db: OpaquePointer?
   
    //default mood set to 'Meh'
    var moodType = "Meh"
    
    //Whenever the user selects each of the moodTypes, the var is updated and the user gets haptic feedback as confirmation that their choice has been made
    @IBAction func awesomeButton(_ sender: Any) {
        moodType = "Awesome"
        getHapticFeedback()
    }
    
    @IBAction func goodButton(_ sender: Any) {
        moodType = "Good"
        getHapticFeedback()
    }
    
    @IBAction func mehButton(_ sender: Any) {
        moodType = "Meh"
        getHapticFeedback()
    }
    
    @IBAction func badButton(_ sender: Any) {
        moodType = "Bad"
        getHapticFeedback()
    }
    
    @IBAction func terribleButton(_ sender: Any) {
        moodType = "Terrible"
        getHapticFeedback()
    }
    
    //The user can enable/disable dark mode by pressing the Dark Mode Button
    @IBOutlet var enterMoodView: UIView!
    @IBOutlet weak var darkModeButton: UIButton!
    
    var darkModeColour = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    var darkModeOn = false
    
    @IBAction func darkModeEnabled(_ sender: Any) {
        getHapticFeedback()
        if(darkModeOn == false){
            enterMoodView.backgroundColor = darkModeColour
            darkModeOn = true
            darkModeColour = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        } else {
            enterMoodView.backgroundColor = darkModeColour
            darkModeOn = false
            darkModeColour = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
    }
    
    @IBOutlet weak var datePickerLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    //When the user changes the datetime on the datepicker, the date label dynamically changes to indicate to the user what their selection is
    func getDatePickerValue(){
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeStyle = DateFormatter.Style.short
        
        let stringDate = dateFormatter.string(from: datePicker.date)
        datePickerLabel.text = stringDate
        
        // current selected date is logged to the console
        print("Date: " + stringDate)
    }
    
    //This function calls the haptic feedback engine
    func getHapticFeedback(){
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
    }
    
    //When the date pciker wheel is updated, the datelable is dynamically updated
    @IBAction func datePickerUpdated(_ sender: Any) {
        getDatePickerValue()
    }
    
    @IBOutlet weak var commentsTextBox: UITextField!
    
    //The user's location is automatically captured without the user having to input anyting
    var locationManager:CLLocationManager!
    
    //This function sets up the location capturing
    func determineMyCurrentLocation(){
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
        }
    }

    //Sets the user's default location
    var userCurrentLocation = "0.0000000000000, 0.0000000000000"
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        //Once the user's location has been captured, the location tracking is stopped
        manager.stopUpdatingLocation()
        
        //The location is captured and passed into the userCurrentLocation variable
        userCurrentLocation = "\(locValue.latitude), \(locValue.longitude)"
        
    }
    
    //If an error occures during location capture, the error is passed to the console
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error){
        print("Error: \(error)")
    }
    
    //Once the user is happy with their choices, the submit button is clicked
    @IBAction func submitButton(_ sender: UIButton) {
        
        //get the user's location
        determineMyCurrentLocation()
        
        //Haptic feedback sent as confirmation
        getHapticFeedback()
        
        let moodDate = datePickerLabel.text!
        let comment = commentsTextBox.text!
        
        var statement: OpaquePointer?
        
        //Preparing SQL queery to insert captured data
        let queryStringSQL = "INSERT INTO Moods (mood_type, mood_date, comment, location) VALUES (?,?,?,?)"
        
        if sqlite3_prepare(db, queryStringSQL, -1, &statement, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("Error preparing insert: \(errmsg)")
            return
        } else {
            print("Success preparing insert")
        }
        
        //All captured Strings are cast as UTF8 Strings, so they can be written/read from the DB
        let UTF8_moodType = moodType as NSString
        
        //Moodtype is bound to the DB. Note the second parameter of sqlite3_bind_text is the second column of the Moods table (index starting from 0)
        if sqlite3_bind_text(statement, 1, UTF8_moodType.utf8String, -1, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            //Failure to bind to the DB is logged to the console
            print("Failure binding moodType: \(errmsg)")
            return
        } else {
            print("Success binding moodType")
        }
        
        let UTF8_moodDate = moodDate as NSString
        
        if sqlite3_bind_text(statement, 2, UTF8_moodDate.utf8String, -1, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("Failure binding moodDate: \(errmsg)")
            return
        } else {
            print("Success binding moodDate")
        }
        
        let UTF8_comment = comment as NSString
        
        if sqlite3_bind_text(statement, 3, UTF8_comment.utf8String, -1, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("Failure binding comment: \(errmsg)")
            return
        } else {
            print("Success binding comment")
        }
        
        let UTF8_userCurrentLocation = userCurrentLocation as NSString
        
        if sqlite3_bind_text(statement, 4, UTF8_userCurrentLocation.utf8String, -1, nil) != SQLITE_OK{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("Failure binding Current Location: \(errmsg)")
            return
        } else {
            print("Success binding Current Location")
        }
        
        //Once all of the user input ha been bound to the Moods table, it is inseted and commited to the table
        if sqlite3_step(statement) != SQLITE_DONE {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            //Failure to write to the DB is logged to the console
            print("Failure inserting mood: \(errmsg)")
            return
        } else {
            //All of the user's inputs are logged to the console, if there was a sucessful insertion
            print("Success inserting mood")
            print("Mood Type: '" + moodType + "' written to database")
            print("Mood Date: '" + moodDate + "' written to database")
            print("Comment: '" + comment + "' written to database")
            print("User Location: '" + userCurrentLocation + "' written to database")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //The current date and time and user's location is captured when they open the app
        getDatePickerValue()
        determineMyCurrentLocation()
        
        //The SQLite file is created in the app storage
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent("Mood_Tracker8.sqlite")
        
        //SQLite file location is logged to the console
        print("SQLite File Location: \(fileURL)")
        
        //Validation that the SQLite file can be opened
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("Error opening database")
        } else {
            print("Success opening database")
        }
        
        //Once the SQLite file is opened, the Moods table will be created if it doesn't already exist
        if sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS Moods (id INTEGER PRIMARY KEY AUTOINCREMENT, mood_type TEXT, mood_date TEXT, comment TEXT, location TEXT)", nil, nil, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            //Validation that the SQLite Moods table was created sucessfully
            print("Error creating table: \(errmsg)")
        } else {
            print("Success creating table")
        }
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }

}
