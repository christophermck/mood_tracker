//
//  UserPreferencesViewController.swift
//  COM555_CW2
//
//  Created by Christopher McKavanagh on 27/03/2018.
//  Copyright © 2018 Christopher McKavanagh. All rights reserved.
//

import UIKit
import UserNotifications

class UserPreferencesViewController: UIViewController {

    //The user can enable/disable dark mode by pressing the Dark Mode Switch
    @IBOutlet var userPreferencesView: UIView!
    
    @IBOutlet weak var darkModeSwitch: UISwitch!
    
    var darkModeColour = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    var defaultColour = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    
    @IBAction func darkModeSwtichClicked(_ sender: Any) {
        if (darkModeSwitch.isOn == true){
            userPreferencesView.backgroundColor = darkModeColour
        } else {
            userPreferencesView.backgroundColor = defaultColour
        }
    }
    
    //The user can enable/disable notifications. A notfication will appear every hour asking the user to input their current mood. A badge is also added to the app icon, to help further remind the user. If the user wishes to turn of nitifications, then they can do so by ficking the notification switch to off
    @IBAction func notificationSwitch(_ sender: Any) {
        let content = UNMutableNotificationContent()
        content.title = "How Are You Felling"
        content.body = "Enter your mood now."
        content.badge = 1
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 60*60, repeats: false)
        //notifiction is launced every hour (60 seconds * 60 minutes)
        let request = UNNotificationRequest(identifier: "timerComplete", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Ask the user if the app can send them notifications
    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler: {didAllow, error in
        })
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

