//
//  SummaryViewController.swift
//  COM555_CW2
//
//  Created by Christopher McKavanagh on 27/03/2018.
//  Copyright © 2018 Christopher McKavanagh. All rights reserved.
//

import UIKit
import SQLite3
import SwiftCharts

class SummaryViewController: UIViewController {

    //initalise the moodType array to capture the occurence of each, stored as a Double
    var moodTypeArrOccDub = 0.0
    
    //For each moodType there is an associated occurence of each
    var mehOcc = 0.0
    var goodOcc = 0.0
    var awesomeOcc = 0.0
    var badOcc = 0.0
    var terribleOcc = 0.0
    
    func readDBValues(){
    
        var statement: OpaquePointer?
        var db: OpaquePointer?
        
        //The SQLite file is created in the app storage
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent("Mood_Tracker8.sqlite")
        
        //Validation that the SQLite file can be opened
        if sqlite3_open(fileURL.path, &db) != SQLITE_OK {
            print("Error opening database")
        } else {
            print("Success opening database")
        }
        
        //Select all from the Moods table
        if sqlite3_prepare_v2(db, "select * from moods", -1, &statement, nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("Error preparing select: \(errmsg)")
        }
        //Array declared for holding all MoodTypes
        var moodTypeArr: Array<String> = Array()
        
        //moodType is declared at set to empty string
        var moodType = ""
        
        //All columns are read form the Moods table and logged to the console for validataion purposes
        while sqlite3_step(statement) == SQLITE_ROW {
            let id = sqlite3_column_int64(statement, 0)
            print("ID = \(id)")
            
            if let cString = sqlite3_column_text(statement, 1) {
                moodType = String(cString: cString)
                moodTypeArr.append(moodType)
                print("Mood Type = \(moodType)")
            } else {
                print("Mood Type not found")
            }
            
            if let cString = sqlite3_column_text(statement, 2) {
                let moodDate = String(cString: cString)
                print("Mood Date = \(moodDate)")
            } else {
                print("Mood Date not found")
            }
            
            if let cString = sqlite3_column_text(statement, 3) {
                let comment = String(cString: cString)
                print("Comment = \(comment)")
            } else {
                print("Comment not found")
            }
            
            if let cString = sqlite3_column_text(statement, 4) {
                let userCurrentLocation = String(cString: cString)
                print("User's Current Location = \(userCurrentLocation)")
            } else {
                print("User's Current Location not found")
            }
        }
        
        //Error message logged to the colsole if there is a problem
        if sqlite3_finalize(statement) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("Error finalizing prepared statement: \(errmsg)")
        }
        
        //Count through the Mood Type Array populated be the above DB read code
        //For each Mood Type, the counter increases by one
        var counts: [String: Int] = [:]
        
        for item in moodTypeArr {
            counts[item] = (counts[item] ?? 0) + 1
        }
        
        //Declaration of array to be used for storing each occurence of each mood type
        var moodTypeArrOcc: Array<Int> = Array()
        
        //This then generates an array of the number of times each Mood Type appears in the database
        for (moodType, occurrences) in counts {
            moodTypeArrOcc.append(occurrences)
            //The Mood Type and number of times is then logged to the console
            print("\(moodType) occurs \(occurrences) time(s)")
        }
        
        //The occurences are then passed into their own vairables, to be used by the charts function later
        mehOcc = Double(moodTypeArrOcc[0])
        goodOcc = Double(moodTypeArrOcc[1])
        awesomeOcc = Double(moodTypeArrOcc[2])
        badOcc = Double(moodTypeArrOcc[3])
        terribleOcc = Double(moodTypeArrOcc[4])
        
        //The largest value must be found, so that the y-axis can be drawn correctly
        moodTypeArrOccDub = Double(moodTypeArrOcc.max()!)
        
    }
    //This function calls the haptic feedback engine
    func getHapticFeedback(){
        let generator = UIImpactFeedbackGenerator(style: .heavy)
        generator.impactOccurred()
    }
    
    //The user can enable/disable dark mode by pressing the Dark Mode Button
    @IBOutlet var summaryView: UIView!
    @IBOutlet weak var darkModeButton: UIButton!
    @IBOutlet weak var graphView: UIView!
    
    var darkModeColour = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    var darkModeOn = false
    
    @IBAction func darkModeEnabled(_ sender: Any) {
        getHapticFeedback()
        if(darkModeOn == false){
            summaryView.backgroundColor = darkModeColour
            graphView.backgroundColor = darkModeColour
            darkModeOn = true
            darkModeColour = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        } else {
            summaryView.backgroundColor = darkModeColour
            graphView.backgroundColor = darkModeColour
            darkModeOn = false
            darkModeColour = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
    }
    
    //I used the Swift Charts library to draw the summary Mood Type chart
    var chart: BarsChart!
    
    @IBOutlet weak var chartView: UIView!
    
    func drawChart(){
        let chartConfig = BarsChartConfig(
            //The y-axis is drawn from 0 to the max value in the moodType array plus one
            valsAxisConfig: ChartAxisConfig(from: 0, to: moodTypeArrOccDub+1, by: 1)
        )
        
        let frame = CGRect(x: 0, y: 70, width: 360, height: 600)
        
        let chart = BarsChart(
            frame: frame,
            chartConfig: chartConfig,
            xTitle: "Mood Types",
            yTitle: "Frequency",
            bars: [
                //The occurence of each Mood Type is then placed into the bar chart
                ("Awesome", awesomeOcc),
                ("Good", goodOcc),
                ("Meh", mehOcc),
                ("Bad", badOcc),
                ("Terrible", terribleOcc),
                ],
            color: UIColor.blue,
            barWidth: 30
        )
        
        self.view.addSubview(chart.view)
        self.chart = chart
        //self.chartView = chart.view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //When the view is loaded, user data is read from the DB and the graph is drawn using this data
        readDBValues()
        
        drawChart()
   
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
